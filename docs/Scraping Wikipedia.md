# Wikipedia Namespaces

ns0 is an Article
ns1 is a User Talk page
ns2 is a User page

ns0,Main/Article
ns1,Talk
ns2,User
ns3,User talk
ns4,Wikipedia
ns5,Wikipedia talk
ns6,File
ns7,File talk
ns8,MediaWiki
ns9,MediaWiki talk
ns10,Template
ns11,Template talk
ns12,Help
ns13,Help talk
ns14,Category
ns15,Category talk
ns100,Portal
ns101,Portal talk
ns118,Draft
ns119,Draft talk
ns710,TimedText
ns711,TimedText talk
ns828,Module
ns829,Module talk
