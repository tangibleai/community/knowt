# Doctests

#### Outline
3. Doctests
  - DDD + TDD
  - `doctest.testmod`
  - `pytest`
  - `gitlab-ci.yml` (GitHub actions not open standard)